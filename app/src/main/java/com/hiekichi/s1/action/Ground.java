package com.hiekichi.s1.action;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

public class Ground {
    private final Paint paint = new Paint();
    final Rect rect;

    public Ground(Rect rect) {
        this.rect = rect;
        paint.setColor(Color.rgb(153, 76, 0));
    }

    public Ground(int left, int top, int right, int bottom) {
        this( new Rect(left, top, right, bottom) );
    }

    public void move(int moveToLeft) {
        rect.offset(-moveToLeft, 0);
    }

    public void draw(Canvas canvas) {
        canvas.drawRect(rect, paint);
    }
}
